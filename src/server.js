import axios from 'axios';

let server = axios.create({
    baseURL: '/api/portfolio',
    withCredentials: true
});

export default server;