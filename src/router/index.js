import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '~/store'

import Projects from '~v/Projects'
import ProjectItem from '~v/ProjectItem'
import Landings from '~v/Landings'
import ELettres from '~v/ELettres'
import E404 from '~v/E404'

Vue.use(VueRouter)


const routes = [
  {
    name: 'home',
    path: '',
    redirect: '/projects'
  },
  {
    path: '/projects',
    name: 'projects',
    component: Projects,
  },
  {
    path: '/projects/:id',
    name: 'project',
    component: ProjectItem,
  },
  {
    path: '/landings',
    name: 'landings',
    component: Landings,
    beforeEnter(to, from, next){
      store.dispatch('landings/load');
      next();
    }
  },
  {
    path: '/eletters',
    name: 'eletters',
    component: ELettres,
    beforeEnter(to, from, next){
      store.dispatch('eletters/load');
      next();
    }
  },
  {
    path: '*',
    component: E404
  }
  
]

const router = new VueRouter({
  mode: 'history',
  routes,
  scrollBehavior () {
    return { x: 0, y: 0 }
  }
})

export default router
