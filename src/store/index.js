import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import projects from './modules/projects';
import landings from './modules/landings';
import eletters from './modules/eletters';


export default new Vuex.Store({
  modules: {
    projects,
    landings,
    eletters
  },
  strict: process.env.NODE_ENV !== 'production'
})
