import server from '~/server'

export default {
    namespaced: true,
    state: {
        items: null,
    },
    getters: {
        loaded: (state) => state.items !== null,
        list(state, getters){
            return state.items
        },
    },
    mutations: {
        set(state, data){
            state.items = data
        },
    },
    actions: {
        load(store){
            server.get('/letters.json').then((res) => {
                if(res.status === 200){
                    store.commit('set', res.data);
                }
            })
        }
    }
}


