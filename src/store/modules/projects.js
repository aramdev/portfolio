import server from '~/server'

export default {
    namespaced: true,
    state: {
        items: null,
    },
    getters: {
        loaded: (state) => state.items !== null,
        list(state, getters){
            return state.items
        },
        item: (state) => (id) => {
            return state.items.find((el) => {
                return el.id == id
            })
        },
    },
    mutations: {
        set(state, data){
            state.items = data
        },
    },
    actions: {
        load(store){
            server.get('/projects.json').then((res) => {
                if(res.status === 200){
                    store.commit('set', res.data);
                }
            })
        }
    }
}


