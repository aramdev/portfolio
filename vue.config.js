let isProd = process.env.NODE_ENV === 'production';
let path = require('path')

let config = {
	productionSourceMap: false,
	configureWebpack: (config) => {
		return {
            resolve: {
                alias: {
                    '~': path.resolve(__dirname, 'src'),
                    '~c': path.resolve(__dirname, 'src/components'),
                    '~v': path.resolve(__dirname, 'src/views'),
                },

            },
            devServer: {
                proxy: {
                    '/api/portfolio': {
                        target: 'http://aramdev.info',
                        secure: false,
                        changeOrigin: true
                    }
                }
                
            }
        }
	}
};



module.exports = config;